const fs = require('fs')
const fetch = require('node-fetch')
const os = require('os')

const mysql = require('mysql');
var con = mysql.createConnection({
  host: "trans.db.freighttiger.com",
  user: "readonly",
  password: "1q2w3e4r",
  database: "TransDB"
});

const googleAutoCompleteUrl = (place) => `http://localhost:8080/v1/place/autocomplete?input=${place}`;
const googleReverseGeocodeUrl = (placeId) => `http://localhost:8080/v1/geocode/reverse?place_id=${placeId}`
const mapMyIndiaUrl = (place) => `https://atlas.mapmyindia.com/api/places/search/json?query=${place}`;
let avg_mmi_time = []
let avg_gg_time = []

const getRequest = async (url, token = null, headers = null) => {
  try {
    const response = await fetch(url, {
      method: 'GET',
      headers: {
        ...headers,
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
      }
    }).then(res => res.json()).then(data => {
      return data;
    });
    return response
  } catch (error) {
    //console.error("failed to fetch api :", url, error)
    throw error;
  }
};

const googleReverseGeocode = async (placeId) => {
  try {
    let token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1Y3YiOnsiaWQiOjQ3NzgsImd1aWQiOiJVU1ItMmYxZTM2YmMtNmI0MC00ZmQ2LWE1MWQtY2EwZDkzNjAzMWNlIiwiZmlyc3RuYW1lIjoiQWRtaW4iLCJsYXN0bmFtZSI6bnVsbCwiZW50aXR5X2d1aWQiOiJDT00tNzJjYjU2NzAtNmFhNi00NDA3LWIzZDItZDI5M2VkZjFhMDZmIiwiY29tcGFueUlkIjoxNTgxLCJncm91cElkIjoiOSIsImVudGl0eV90eXBlIjoiQ05SIiwiY29tcGFueVR5cGUiOlt7Im5hbWUiOiJDb25zaWdub3IiLCJrZXkiOiJjb25zaWdub3IifV19LCJwZXJtaXNzaW9ucyI6eyJwZXJtaXNzaW9uX2NyZWF0ZV9tYXNrIjpbMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDBdLCJwZXJtaXNzaW9uX3VwZGF0ZV9tYXNrIjpbMzIsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMTA3Mzc0MTgyNCwwXSwicGVybWlzc2lvbl9yZWFkX21hc2siOlszMiwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwxMDczNzQxODI0LDFdfSwiZGVza0lkIjoiRFNLLTE3MDZjM2MwLWZjYTUtNDYzNi1hODA3LWEyYTJkMTljZmJiYyIsImlhdCI6MTU4ODY3NDE0NiwibmJmIjoxNTg4Njc0MTQ2LCJleHAiOjE1ODg3NjA1NDYsImlzcyI6Imh0dHA6Ly9waG9lbml4L2FwaS9hdXRoZW50aWNhdGUiLCJzdWIiOiI0Nzc4IiwianRpIjoiYjI4Zjg3ODUtMTdmYy00ZGM2LThlNDItNzZiNWViYWU3ZjQ3In0.Zku7HbSFXkQM3C57eu6y24e4nQsBK0fvFrOe1X3zClc'
    const response = await getRequest(googleReverseGeocodeUrl(placeId), token)
    return response.data.results[0].geometry.location
  } catch (err) {
    //console.error('googleReverseGeocode: ', placeId, ' **  ', err, '\n')
    return null
  }
}

const googleAutocompleteApi = async (place) => {
  try {
    let token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1Y3YiOnsiaWQiOjQ3NzgsImd1aWQiOiJVU1ItMmYxZTM2YmMtNmI0MC00ZmQ2LWE1MWQtY2EwZDkzNjAzMWNlIiwiZmlyc3RuYW1lIjoiQWRtaW4iLCJsYXN0bmFtZSI6bnVsbCwiZW50aXR5X2d1aWQiOiJDT00tNzJjYjU2NzAtNmFhNi00NDA3LWIzZDItZDI5M2VkZjFhMDZmIiwiY29tcGFueUlkIjoxNTgxLCJncm91cElkIjoiOSIsImVudGl0eV90eXBlIjoiQ05SIiwiY29tcGFueVR5cGUiOlt7Im5hbWUiOiJDb25zaWdub3IiLCJrZXkiOiJjb25zaWdub3IifV19LCJwZXJtaXNzaW9ucyI6eyJwZXJtaXNzaW9uX2NyZWF0ZV9tYXNrIjpbMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDBdLCJwZXJtaXNzaW9uX3VwZGF0ZV9tYXNrIjpbMzIsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMTA3Mzc0MTgyNCwwXSwicGVybWlzc2lvbl9yZWFkX21hc2siOlszMiwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwxMDczNzQxODI0LDFdfSwiZGVza0lkIjoiRFNLLTE3MDZjM2MwLWZjYTUtNDYzNi1hODA3LWEyYTJkMTljZmJiYyIsImlhdCI6MTU4ODY3NDE0NiwibmJmIjoxNTg4Njc0MTQ2LCJleHAiOjE1ODg3NjA1NDYsImlzcyI6Imh0dHA6Ly9waG9lbml4L2FwaS9hdXRoZW50aWNhdGUiLCJzdWIiOiI0Nzc4IiwianRpIjoiYjI4Zjg3ODUtMTdmYy00ZGM2LThlNDItNzZiNWViYWU3ZjQ3In0.Zku7HbSFXkQM3C57eu6y24e4nQsBK0fvFrOe1X3zClc'
    const response = await getRequest(googleAutoCompleteUrl(place), token)

    let placeId = response.data.predictions[0].place_id ? response.data.predictions[0].place_id : '1234'
    let placeName = response.data.predictions[0].description ? response.data.predictions[0].description : 'Bengaluru'
    //console.log('Place id --------', placeName)

    let latlng = await googleReverseGeocode(placeId)

    return {
      placeName,
      ...latlng
    }
  } catch (err) {
    //console.error('googleAutocompleteApi: ', place, ' -- ', err, '\n')
    return null
  }
}

const mapMyIndiaApi = async (place) => {
  try {
    console.log(place)
    const token = '562050c1-79f3-43d8-93db-655ae8bd6bc4';
    let start = new Date()
    const response = await getRequest(mapMyIndiaUrl(place), token);
    let mmi_time = new Date() - start
    let placeName = response.suggestedLocations[0].placeName ? response.suggestedLocations[0].placeName : ' '
    console.log('Place Name------------->', placeName)
    start = new Date()
    let {
      placeName: gg_name,
      lat: gg_lat,
      lng: gg_lng
    } = await googleAutocompleteApi(place)
    let gg_time = new Date() - start
    const returnObj = {
      mmi_name: placeName.split(',').join(';'),
      mmi_lat: response.suggestedLocations[0].latitude,
      mmi_lng: response.suggestedLocations[0].longitude,
      mmi_time,
      gg_name: gg_name.split(',').join(';'),
      gg_lat,
      gg_lng,
      gg_time
    };
    avg_gg_time.push(gg_time)
    avg_mmi_time.push(mmi_time)
    return returnObj;
  } catch (err) {
   // console.error(place, err);
    return null;
  }
}

const getDifferenceFromLatLng = (lat1, long1, lat2, long2) => {
  //radians
  lat1 = (lat1 * 2.0 * Math.PI) / 60.0 / 360.0;
  long1 = (long1 * 2.0 * Math.PI) / 60.0 / 360.0;
  lat2 = (lat2 * 2.0 * Math.PI) / 60.0 / 360.0;
  long2 = (long2 * 2.0 * Math.PI) / 60.0 / 360.0;

  // Haversine formula  
  const dlon = long2 - long1;
  const dlat = lat2 - lat1;
  const a = Math.pow(Math.sin(dlat / 2), 2) +
    Math.cos(lat1) * Math.cos(lat2) *
    Math.pow(Math.sin(dlon / 2), 2);

  const c = 2 * Math.asin(Math.sqrt(a));
  const r = 6371; //earth radius
  return r * c;
}

const avgTimeRow = () => {
  avg_mmi = avg_mmi_time.reduce((acc, curr) => acc + curr)/avg_mmi_time.length;
  avg_gg = avg_gg_time.reduce((acc, curr) => acc + curr)/avg_gg_time.length;
  const responseRow = [].concat('Avg API response time for MapMyIndia').concat(avg_mmi).concat('Avg API response time for Google').concat(avg_gg)
  return responseRow;
}

const writeDataToCsv = (dataToWrite) => {
  console.log(dataToWrite)
  dataToWrite = dataToWrite.filter(ele => ele)
  let output = [];
  output.push(Object.keys(dataToWrite[0]).concat('deviation(in KM)').join());
  dataToWrite.forEach(element => {
    const diff = getDifferenceFromLatLng(element.mmi_lat, element.mmi_lng, element.gg_lat, element.gg_lng)
    output.push(Object.values(element).concat(diff).join())
  });
  output.push(avgTimeRow())
  fs.writeFile("report.csv", output.join(os.EOL), (err) => {
    if (err) {
      return //console.log(err);
    }
    //console.log("The file was saved!")
  })
}

const dbRead = async () => {
  con.connect(function (err) {
    if (err) throw err;
    con.query(`SELECT id,loadingPointInfo FROM LorryReceipts where ID IN ('2279399','2279417','2279414','2279399','2279396','2279381','2279378','2279416','2279367','2279362','2279346','2279340','2279331','2279274','2279246','2279232','2279215','2279214','2279209','2279163','2279091')
    `, async (err, result, fields) => {
      if (err) throw err;
      const final = result.map(async place => {
        return await mapMyIndiaApi(place.loadingPointInfo.split(',')[0]);
      })
      return Promise.all(final).then(dataToWriteToFile => writeDataToCsv(dataToWriteToFile)).catch(err => console.error('All: ', err))
    });
    con.end();
  });
}


(async () => {
  dbRead()
})()