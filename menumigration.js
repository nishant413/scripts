const fs = require('fs')
const fetch = require('node-fetch')
const os = require('os')
var XLSX = require('xlsx')
var workbook = XLSX.readFile('Permissions for Factory Role.xlsx');


const mysql = require('mysql');
var con = mysql.createConnection({
    host: "trans.db.freighttiger.com",
    user: "readonly",
    password: "1q2w3e4r",
    database: "TransDB"
});

const migration = () => {
    con.connect(function (err) {
        if (err) throw err;
        con.query(`Select name,slug,url from menu where slug in ('mytripsNeo','addTripNeo','report','docUploadNeo','controlTowerNeo')`, async (err, result, fields) => {
            if (err) throw err;
            const final = result.map(async menu => {
                return await migrateToMenuConfig(menu);
            })
        });
        con.end();
    });
}

const migrateToMenuConfig = async (menu) => {
    try {
        let body = await buildBodyForMenuConfig(menu)
        console.log(body)
        let appId = 'phoenix.dev.freighttiger.com'
        let headers = {
            "x-ft-userid": "USR-19e95233-4f5a-49ff-96d1-48b3075e975e"
        }
        let url = `http://client-config-service.dev.freighttiger.com/v1/config/admin/menu/app/${appId}?menuKey=hamburger`
        let response = await postRequest(url, body,headers)
        console.log(response)
    } catch (err) {
        throw err
    }


}

buildBodyForMenuConfig = async (menu) => {
    // let item_permission = await readPermissionfromExcel(menu)
    let body = {
        "menu_key": "hamburger",
        "item_key": menu.slug,
        "item_metadata": {
            "item_icon": "Not availablr",
            "item_title": menu.name,
            "item_action": menu.url,
            "item_permissions": []
        },
        "feature_configs": [menu.slug],
    }
    return body;
}

readPermissionfromExcel = async (menu) => {

    var sheet_name_list = workbook.SheetNames;
    var xlData = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);
    console.log(xlData);
}


const postRequest = async (url,body={}, headers = null, token = null) => {
    try {
        const response = await fetch(url, {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                ...headers,
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            return data;
        });
        return response
    } catch (error) {
        //console.error("failed to fetch api :", url, error)
        throw error;
    }
};

(async () => {
    migration()
})()