const fs = require('fs')
const util = require('util');
const fetch = require('node-fetch')
const os = require('os')

const mysql = require('mysql');
const con = mysql.createConnection({
    host: "10.10.98.57",
    user: "root",
    password: "password",
    database: "client_config_service"
});

const query = util.promisify(con.query).bind(con);

const pg = require('pg')
const client = new pg.Client({
    host: "10.10.86.234",
    user: "root",
    password: "password",
    database: "access_control_service"
});


const migration = async () => {
    try {
        await client.connect();
        con.connect();
        const menuidList = await query(`Select menu_id from menu_configs where app_id="phoenix.dev.freighttiger.com"`);
        let menuids = menuidList.map(({
            menu_id
        }) => menu_id)
        
        const roleIds = await client.query('select role_id from role')
        let roles = roleIds.rows.map(async role =>
            await migrateMenuOrder(role,menuids))
        await client.end()
    } catch (err) {
        throw err
    }
}

const migrateMenuOrder = async (role,menuids) => {
    try {
        console.log(role)
        let body = {
            "menu_order":menuids
        }
         console.log(body)
         let headers = {
            "x-ft-userid": "USR-19e95233-4f5a-49ff-96d1-48b3075e975e"
        }
        let url = `http://client-config-service.dev.freighttiger.com/v1/config/role/menu/order/hamburger/${role.role_id}`
        console.log(url)
        let response = await postRequest(url,body,headers)
        // console.log(response)
    } catch (err) {
        throw err
    }


}


const postRequest = async (url, body = {}, headers = null, token = null) => {
    try {
        const response = await fetch(url, {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                ...headers,
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        }).then(res => {console.log(res); return res.json()}).then(data => {
            console.log(data)
            return data;
        }).catch(err => console.log(err,'\n\n\n\n\n'));
        return response
    } catch (error) {
        //console.error("failed to fetch api :", url, error)
        throw error;
    }
};

(async () => {
    migration()
})()